package com.example.statisticservice.feignclient;

import static org.springframework.cloud.openfeign.security.OAuth2AccessTokenInterceptor.AUTHORIZATION;

import com.example.statisticservice.dto.TransactionResponseDto;
import java.util.List;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestHeader;

@FeignClient(value = "transaction-service", url = "http://localhost:9003/auth/transactions")
public interface TransactionClient {

  @GetMapping("/all")
  List<TransactionResponseDto> getAllTransactions(@RequestHeader(AUTHORIZATION) String bearerToken);
}
