package com.example.statisticservice.enums;

public enum TransactionType {
    BUY,
    SELL,
    FIX
}
