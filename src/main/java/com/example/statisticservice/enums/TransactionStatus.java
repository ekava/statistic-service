package com.example.statisticservice.enums;

public enum TransactionStatus {
    ACCEPTED,
    DECLINED,
    PROCESSING
}
