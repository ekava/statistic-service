package com.example.statisticservice.dto;

import com.example.statisticservice.enums.TransactionStatus;
import com.example.statisticservice.enums.TransactionType;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.UUID;

public record TransactionResponseDto(UUID id,
                                     LocalDateTime date,
                                     UUID clientId,
                                     UUID itemId,
                                     BigDecimal price,
                                     TransactionType type,
                                     TransactionStatus status) {
}