package com.example.statisticservice.config.properties;

import jakarta.annotation.PostConstruct;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Getter
@Setter
@Slf4j
@Configuration
@ConfigurationProperties(prefix = "env.keycloak")
public class KeycloakProperties {
  private String serverIp;
  private String port;
  private String realmName;
  private String clientId;
  private String clientSecret;
  private String url;
  private String grantType;
  private String userRoleId;
  private String containerId;

  @PostConstruct
  protected void printKeycloakProperties() {
    if (log.isDebugEnabled()) {
      log.debug(
          """
                    {
                        serverIp: {},
                        port: {},
                        realmName: {},
                        clientId: {},
                        clientSecret: {},
                        grantType: {},
                        url: {},
                        userRoleId: {},
                        containerId: {}
                    }
                    """,
          serverIp,
          port,
          realmName,
          clientId,
          clientSecret,
          grantType,
          url,
          userRoleId,
          containerId);
    }
    if (clientSecret == null || clientSecret.isBlank() || clientSecret.equals("${CLIENT_SECRET}")) {
      log.error(
          "You have to provide CLIENT_SECRET in application.yaml or in environmental variables to make program work properly.");
    }
  }
}
