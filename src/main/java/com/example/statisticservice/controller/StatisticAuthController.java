package com.example.statisticservice.controller;

import com.example.statisticservice.service.StatisticService;
import jakarta.annotation.security.RolesAllowed;
import java.math.BigDecimal;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.oauth2.jwt.Jwt;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/auth/statistics")
@RequiredArgsConstructor
public class StatisticAuthController {

  private final StatisticService statisticService;

  @GetMapping("/profit")
  @RolesAllowed("owner")
  public BigDecimal getProfit(@AuthenticationPrincipal Jwt jwt) {
    return statisticService.getProfit(jwt.getTokenValue());
  }
}
