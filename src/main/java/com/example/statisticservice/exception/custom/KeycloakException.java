package com.example.statisticservice.exception.custom;

import org.springframework.http.HttpStatus;

public class KeycloakException extends BadRequestException {
    public static KeycloakException UNABLE_TO_GET_ADMIN_TOKEN =
            new KeycloakException(HttpStatus.BAD_REQUEST, "Unable to get admin token from keycloak.");

    public static KeycloakException UNABLE_TO_REGISTER_USER_IN_KEYCLOAK =
            new KeycloakException(HttpStatus.BAD_REQUEST, "Unable to register user in keycloak.");

    public static KeycloakException UNABLE_TO_FIND_USER_IN_KEYCLOAK =
            new KeycloakException(HttpStatus.BAD_REQUEST, "Unable to find user in keycloak.");

    public KeycloakException(HttpStatus status, String reason) {
        super(status, reason);
    }
}
