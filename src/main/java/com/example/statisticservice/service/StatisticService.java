package com.example.statisticservice.service;

import java.math.BigDecimal;

public interface StatisticService {

  BigDecimal getProfit(String bearerToken);
}
