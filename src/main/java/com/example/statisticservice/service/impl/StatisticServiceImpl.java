package com.example.statisticservice.service.impl;

import static com.example.statisticservice.enums.TransactionType.SELL;

import com.example.statisticservice.dto.TransactionResponseDto;
import com.example.statisticservice.enums.TransactionType;
import com.example.statisticservice.feignclient.TransactionClient;
import com.example.statisticservice.service.StatisticService;
import java.math.BigDecimal;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class StatisticServiceImpl implements StatisticService {

  private final TransactionClient transactionClient;

  @Override
  public BigDecimal getProfit(String bearerToken) {
    var transactions = transactionClient.getAllTransactions("Bearer " + bearerToken);

    return transactions.stream()
        .map(transaction -> transaction.price().multiply(transaction.type().equals(SELL) ? BigDecimal.ONE : BigDecimal.ONE.negate()))
        .reduce(BigDecimal.ZERO, BigDecimal::add);
  }
}
